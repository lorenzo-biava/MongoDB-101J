# MongoDB 101J

Assignments with material and solutions from the 2016 edition of the MongoDB University [M101J course](https://university.mongodb.com/courses/MongoDB/M101J/2016_October/syllabus).
Completion confirmation [here](http://university.mongodb.com/course_completion/4567244fbc1a484e9b0581dbe34c2d07).
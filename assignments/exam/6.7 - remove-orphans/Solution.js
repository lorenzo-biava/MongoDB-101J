/*
Your task is to write a program to remove every image from the images collection that appears in no album.
Or put another way, if an image does not appear in at least one album, it's an orphan and should be removed from the images collection.

When you are done removing the orphan images from the collection, there should be 89,737 documents in the images collection.

Hint: you might consider creating an index or two or your program will take a long time to run. As as a sanity check, there are 49,887 images that are tagged 'sunrises' before you remove the images.

What are the total number of images with the tag "sunrises" after the removal of orphans?
*/

use photoshare

// Add an index to Albums for the images, otherwise the Albums.find query will be very slow!!
db.albums.ensureIndex({"images":1});

// Iterate through every image
var cursor = db.images.find();

var numRemoved = 0;

while(cursor.hasNext()) {
	image = cursor.next();
	imageId = image._id;	

	// Find albums with that image in them
	numAlbums = db.albums.find({"images":imageId}).count();
	// Remove image that are not in any album
	if (numAlbums==0) {
		numRemoved++;
		print("Removed image : " + imageId)
		db.images.remove({"_id":imageId})
	}
}

print("Removed " + numRemoved + " images");

numSunrises = db.images.find({"tags":"sunrises"}).count();
print("Total number of images with the tag 'sunrises': " + numSunrises);
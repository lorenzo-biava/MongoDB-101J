package course;

import java.io.IOException;
import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;

public class Main {
	public static void main(String[] args) throws IOException {
		String mongoURIString;
		if (args.length == 0) {
			mongoURIString = "mongodb://localhost";
		} else {
			mongoURIString = args[0];
		}

		MongoClient mongoClient = new MongoClient(new MongoClientURI(
				mongoURIString));
		MongoDatabase db = mongoClient.getDatabase("school");
		MongoCollection<Document> students = db
				.getCollection("students");

		// DBObject unwind = new BasicDBObject("$unwind", "$scores");
		// DBObject match = new BasicDBObject("$match", new
		// Document("scores.type", "homework"));
		// DBObject sort = new BasicDBObject("$sort", new
		// BasicDBObject("scores.score":-1));

		// students.aggregate(Arrays.asList(unwind, match,sort));

		AggregateIterable<Document> result = students.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(new Document("scores.type", "homework")),
				Aggregates.sort(new Document("scores.score", 1))));

		for (Document student : result) {// students.find()){
			System.out.println(student);
		}

//		students.find(new Document("scores.type", "homework")).forEach(
//				new Block<Document>() {
//					public void apply(final Document document) {
//
//						Collections.sort(
//								(List<Document>) document.get("scores"),
//								new Comparator<Document>() {
//									public int compare(Document o1, Document o2) {
//										double diff = o2.getDouble("score")
//												- o1.getDouble("score");
//										if (diff < 0)
//											return -1;
//										else if (diff > 0)
//											return 1;
//										else
//											return 0;
//
//									}
//								});
//
//						float lowestScore = 1;
//						/*students.updateOne(
//								new Document("_id", document.get("_id")),
//								Updates.pull("score", lowestScore));*/
//
//						// var lowestHomeworkScore =
//						// sortedScores[sortedScores.length-1].score;
//						// db.students.update({_id: s._id},{$pull: {scores:
//						// {score: lowestHomeworkScore}}}, {multi: true});
//
//						System.out.println("Deleting score: "+ lowestScore+" from doc: ",document.get("_id"));
//					}
//				});

		for (Document student : result) {// students.find()){
			System.out.println(student);
		}
		System.out.println("END");
	}
}
